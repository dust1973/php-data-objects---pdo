<?php


class UsersRepository
{


    /**
     * @var null|PDO
     */
    private $db = null;

    /**
     * UsersRepository constructor.
     */
    public function __construct($db)
    {
        $this->db = $db;
    }


    /**
     * @param $username
     * @return array
     */
    public function getUser($username)
    {
        $sql = "SELECT * FROM users WHERE username = :username";

        $abfrage = $this->db->prepare($sql);
        $abfrage->bindParam(':username', $username, PDO::PARAM_STR);
        $abfrage->execute();


        /* Fetching an array without objects*/
        // $res = $abfrage->fetch();
        /*  Result: */
        /*  array (size=8)
            'uid' => string '1' (length=1)
             0 => string '1' (length=1)
             'username' => string 'fuchsa' (length=6)
             1 => string 'fuchsa' (length=6)
             'firstname' => string 'Alexander' (length=9)
             2 => string 'Alexander' (length=9)
             'lastname' => string 'Fuchs' (length=5)
             3 => string 'Fuchs' (length=5)
        */


        /* To create a single object from the query results you have two options. You can use the either a familiar fetch() method:*/
        // $abfrage->setFetchMode(PDO::FETCH_CLASS, 'User');
        // $res = $abfrage->fetch();
        /*  Result:
            object(User)[4]
            protected 'uid' => string '1' (length=1)
            protected 'username' => string 'fuchsa' (length=6)
            protected 'firstname' => string 'Alexander' (length=9)
            protected 'lastname' => string 'Fuchs' (length=5)
        */

        /* or a dedicated fetchObject() method:*/
        $res = $abfrage->fetchObject('User');
        /*  Result:
            object(User)[4]
            protected 'uid' => string '1' (length=1)
            protected 'username' => string 'fuchsa' (length=6)
            protected 'firstname' => string 'Alexander' (length=9)
            protected 'lastname' => string 'Fuchs' (length=5)
        */

        $errors = $abfrage->errorInfo();

        return $res;

    }

    /**
     *
     * @return array
     */
    public function getUsers()
    {
        $sql = "SELECT * FROM users";

        $abfrage = $this->db->prepare($sql);
        $abfrage->execute();
        $abfrage->setFetchMode(PDO::FETCH_CLASS, 'User');
        $res = $abfrage->fetchAll();
        return $res;

    }

    /**
     * @return int
     * @var User $user
     */
    public function insertUser($user)
    {
        /*INSERT query with positional placeholders*/
        //$sql = "INSERT INTO users (username, firstname, lastname) VALUES (?, ?, ?)";

        /*INSERT query with named placeholders*/
        $sql = "INSERT INTO users (username, firstname, lastname) VALUES (:username, :firstname, :lastname)";
        $data = array();
        $data['username'] = $user->getUsername();
        $data['firstname'] = $user->getFirstname();
        $data['lastname'] = $user->getLastname();

        $abfrage = $this->db->prepare($sql);

        /*INSERT query with positional placeholders*/
        //$abfrage->execute([$user->getUsername(), $user->getFirstname(), $user->getLastname()]);

        $abfrage->execute($data);

        $errors = $abfrage->errorInfo();
        $last_id = $this->db->lastInsertId();

        return $last_id;

    }

    /**
     * @return boolean
     * @var User $user
     */
    public function updateUser($user)
    {
        $data = [
            $user->getUsername(),
            $user->getFirstname(),
            $user->getLastname(),
            $user->getUid()
        ];

        $sql = "UPDATE users SET username=?, firstname=?, lastname=? WHERE uid=?";
        $abfrage = $this->db->prepare($sql);
        $res = $abfrage->execute($data);

        return $res;
    }
}