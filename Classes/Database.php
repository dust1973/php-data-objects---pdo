<?php

/**
 * Created by PhpStorm.
 * User: FuchsA
 * Date: 09.09.2015
 * Time: 13:38
 */

include_once './Config.php';


class Database
{
    /**
     * @var null|PDO
     */
    private $db = null;
    /**
     * @var string
     */
    private $passwort = Config::PASSWORT;
    /**
     * @var string
     */
    private $host = Config::HOST;
    /**
     * @var string
     */
    private $dbname = Config::DBNAME;
    /**
     * @var string
     */
    private $dbuser = Config::DBUSER;


    /**
     *
     */
    function connectDB()
    {
        $optionen = array(
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_BOTH
        );

        try {
            //for MSSQL "sqlsrv:Server=10.2.10.106;Database="
            //for MySQL "mysql:host=localhost;dbname="
            $this->db = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->dbname, $this->dbuser, $this->passwort, $optionen);
            return $this->db;

        } catch (PDOException $e) {
            echo 'Connection failed: ' . $e->getMessage();
            exit;
        }

        return false;

    }

}